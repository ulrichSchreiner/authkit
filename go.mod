module gitlab.com/ulrichSchreiner/authkit

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.1.0
	golang.org/x/net v0.0.0-20180511174649-2491c5de3490
	golang.org/x/oauth2 v0.0.0-20180503012634-cdc340f7c179
	google.golang.org/appengine v1.0.0
)
