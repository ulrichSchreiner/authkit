package authkit

// some constants
const (
	Facebook Provider = "facebook"
	Google            = "google"
	Github            = "github"
	Live              = "live"
	LinkedIn          = "linkedin"
)

// A ProviderRegistry contains all registerd providers.
type ProviderRegistry map[Provider]AuthRegistration

var (
	defaultBackends = ProviderRegistry{
		Facebook: AuthRegistration{
			Network:        Facebook,
			Scopes:         []string{"public_profile", "email"},
			AuthURL:        "https://www.facebook.com/dialog/oauth/",
			AccessTokenURL: "https://graph.facebook.com/oauth/access_token",
			AccessType:     "offline",
			UserinfoBase:   "https://graph.facebook.com/v3.0",
			UserinfoURLs:   []string{"/me?fields=email,name,picture"},
			PathID:         "url[0].id",
			PathEMail:      "url[0].email",
			PathName:       "url[0].name",
			PathPicture:    "url[0].picture.data.url",
			PathCover:      "",
		},
		Google: AuthRegistration{
			Network:        Google,
			Scopes:         []string{"openid", "profile", "email"},
			AuthURL:        "https://accounts.google.com/o/oauth2/auth",
			AccessTokenURL: "https://accounts.google.com/o/oauth2/token",
			AccessType:     "offline",
			UserinfoBase:   "https://www.googleapis.com",
			UserinfoURLs:   []string{"/oauth2/v2/userinfo"},
			PathID:         "url[0].id",
			PathEMail:      "url[0].email",
			PathName:       "url[0].name",
			PathPicture:    "url[0].picture",
			PathCover:      "",
		},
		Github: AuthRegistration{
			Network:        Github,
			Scopes:         []string{"user:email"},
			AuthURL:        "https://github.com/login/oauth/authorize",
			AccessTokenURL: "https://github.com/login/oauth/access_token",
			AccessType:     "offline",
			UserinfoBase:   "https://api.github.com",
			UserinfoURLs:   []string{"/user", "/user/emails"},
			PathID:         "url[0].login",
			PathEMail:      "url[1].data[0].email",
			PathName:       "url[0].name",
			PathPicture:    "url[0].avatar_url",
			PathCover:      "",
		},
		Live: AuthRegistration{
			Network:        Live,
			Scopes:         []string{"openid", "email", "profile"},
			AuthURL:        "https://login.microsoftonline.com/common/oauth2/v2.0/authorize",
			AccessTokenURL: "https://login.microsoftonline.com/common/oauth2/v2.0/token",
			AccessType:     "code",
			UserinfoBase:   "https://graph.microsoft.com/",
			UserinfoURLs:   []string{"/oidc/userinfo"},
			PathID:         "url[0].id",
			PathEMail:      "url[0].email",
			PathName:       "url[0].name",
			PathPicture:    "",
			PathCover:      "",
		},
		LinkedIn: AuthRegistration{
			Network:        LinkedIn,
			Scopes:         []string{"r_basicprofile", "r_emailaddress"},
			AuthURL:        "https://www.linkedin.com/uas/oauth2/authorization",
			AccessTokenURL: "https://www.linkedin.com/uas/oauth2/accessToken",
			AccessType:     "offline",
			UserinfoURLs:   []string{"/v1/people/~:(picture-url,first-name,last-name,id,formatted-name,email-address)?format=json"},
			UserinfoBase:   "https://api.linkedin.com",
			PathID:         "url[0].id",
			PathEMail:      "url[0].emailAddress",
			PathName:       "url[0].formattedName",
			PathPicture:    "url[0].pictureUrl",
			PathCover:      "",
		},
	}
)

// GetRegistry returns a registry description for the given backend or an
// empty registration block.
func GetRegistry(backend Provider) AuthRegistration {
	res, _ := defaultBackends[backend]
	return res
}

// Instance returns a new registration provider with the given clientid
// and clientsecret.
func Instance(backend Provider, clientid, clientsecret string, scopes ...string) AuthRegistration {
	b := GetRegistry(backend)
	b.ClientID = clientid
	b.ClientSecret = clientsecret
	if scopes != nil && len(scopes) > 0 {
		b.Scopes = scopes
	}
	return b
}

// FillDefaults fills the given registration struct with the default values
// from the backend. The values are only overwritten if they are empty.
func FillDefaults(backend Provider, reg AuthRegistration) AuthRegistration {
	def := GetRegistry(backend)
	if reg.Scopes == nil || len(reg.Scopes) == 0 {
		reg.Scopes = def.Scopes
	}
	if reg.AuthURL == "" {
		reg.AuthURL = def.AuthURL
	}
	if reg.AccessTokenURL == "" {
		reg.AccessTokenURL = def.AccessTokenURL
	}
	if len(reg.UserinfoURLs) == 0 {
		reg.UserinfoURLs = def.UserinfoURLs
	}
	if reg.PathID == "" {
		reg.PathID = def.PathID
	}
	if reg.PathEMail == "" {
		reg.PathEMail = def.PathEMail
	}
	if reg.PathName == "" {
		reg.PathName = def.PathName
	}
	if reg.PathPicture == "" {
		reg.PathPicture = def.PathPicture
	}
	if reg.PathCover == "" {
		reg.PathCover = def.PathCover
	}
	return reg
}
