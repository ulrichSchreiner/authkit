package authkit

import (
	"html/template"
	"strings"
)

var funcMap = template.FuncMap{
	"join": strings.Join,
}

var loginTemplate = template.Must(template.New("login").Funcs(funcMap).Parse(loginJS))
var loginJS = `
var __authkit__state__ = {
  providers : {}
};

{{ $base := .Base }}
{{ range $key, $value := .Providers }}
__authkit__state__.providers["{{ $value.Network }}"] = {
  network : "{{ $value.Network }}",
  authurl : "{{ $value.AuthURL }}",
  client_id : "{{ $value.ClientID }}",
  scopes : "{{ join $value.Scopes " " }}",
  accesstype: "{{ $value.AccessType }}",
  redirect: "{{ $base }}redirect"
};
{{ end }}

var authkit = {
  providers : function () {
    return __authkit__state__.providers;
  },
  provider : function (p) {
    return __authkit__state__.providers[p];
  },
  login : function (provider,base) {
    var p = authkit.provider(provider);
    if (!p) return;
	if (!base) base = window.location.origin;
	var randVal = parseInt(Math.random()*1e12,20).toString(36);
	document.cookie = "authkitval="+randVal+"; max-age=300";
    var scopes = encodeURIComponent(p.scopes);
    var redir = base+p.redirect;
	var state = encodeURIComponent(window.btoa(JSON.stringify({rval:randVal, network:p.network,redirect_uri:redir})));
    var authU = p.authurl+"?prompt=select_account&redirect_uri="+redir+"&response_type=code&client_id="+p.client_id+"&state="+state;
    if (p.scopes) {
      authU = authU + "&scope="+scopes;
    }
    if (p.accesstype) {
      authU = authU + "&access_type="+p.accesstype;
    }
    var self = this;
	self.user = function (cb) {
		document.cookie = "authcbname="+cb+"; max-age=300";
		doLogin(authU);
	};
    return self;
  }
};

window.onload = function(e){
	if (window.location.hash) {
		var f = window[window.location.hash.substring(1)];
		f();
	}
};

function doLogin (u) {
	window.location.href = u;
}

var tokenkey = "usr-token";
var userkey = "usr-user";
var errorkey = "usr-error";

function returnOauth () {
	var token = localStorage.getItem(tokenkey);
	var usr = JSON.parse(localStorage.getItem(userkey));
	var err = localStorage.getItem(errorkey);

	localStorage.removeItem(tokenkey);
	localStorage.removeItem(userkey);
	localStorage.removeItem(errorkey);

	var cbname = document.cookie.replace(/(?:(?:^|.*;\s*)authcbname\s*\=\s*([^;]*).*$)|^.*$/, "$1");
	if (!cbname) return;
	var f = window[cbname];
	if (!f) {
		console.error("the callback: ", cbname," is undefined")
		return
	}
	if (token && usr) {
		f(usr, token);
	} else if (err) {
		f(null, null, err);
	}
}
`
var redirectTemplate = template.Must(template.New("redirect").Funcs(funcMap).Parse(redirect))
var redirect = `
<html>
<body>
  <script>
  var tokenkey = "usr-token";
  var userkey = "usr-user";
  var errorkey = "usr-error";

  // First, parse the query string
    var params = {}, queryString = location.search.substring(1),
        regex = /([^&=]+)=([^&]*)/g, m;
    while (m = regex.exec(queryString)) {
      params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

	var req = new XMLHttpRequest();
	var pstate = JSON.parse(window.atob(params.state));

	var authok = false;
	if (document.cookie.split(';').filter(function(item) {
		return item.indexOf('authkitval='+pstate.rval) >= 0
	}).length) {
		authok = true;
	}

    req.open('GET', '{{ .Base }}auth?code='+params.code+"&state="+params.state, true);

    req.onreadystatechange = function (e) {
      if (req.readyState == 4) {
        if(req.status == 200){
			if (authok) {
				var tok = req.getResponseHeader("Authorization");
				var usr = req.responseText;
				localStorage.setItem(tokenkey, tok);
				localStorage.setItem(userkey, usr);
 				window.location.href="/#returnOauth";
			}
        }
        else if(req.status == 400) {
            console.error('There was an error processing the access code:',req.responseText)
			localStorage.setItem(errorkey, req.responseText);
            window.location.href="/#returnOauth";
        }
        else {
        	console.error('something other than 200 was returned:',req.responseText)
			localStorage.setItem(errorkey, req.responseText);
			window.location.href="/#returnOauth";
        }
      }
    };
    req.send(null);
	</script>
</body>
</html>
`
